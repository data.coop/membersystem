run:
    @echo "Running the server"
    docker compose up --watch --remove-orphans

manage *ARGS:
    @echo "Running manage command"
    docker compose run -w /app/src --rm -u `id -u` app python manage.py {{ARGS}}

build:
    @echo "Building the app"
    docker compose build

down_with_volumes:
    @echo "Taking the docker compose stack down, including volumes"
    docker compose down -v

migrate:
    @echo "Migrating the database"
    docker compose run -w /app/src --rm -u `id -u` app python manage.py migrate

bootstrap_dev_data:
    @echo "Bootstrapping development data"
    docker compose run -w /app/src --rm -u `id -u` app python manage.py bootstrap_dev_data

reset_db: down_with_volumes migrate bootstrap_dev_data

typecheck:
    docker compose run -w /app/src --rm app mypy .

test:
    docker compose run --rm app pytest

# You need to install Stripe CLI from here to run this: https://github.com/stripe/stripe-cli/releases
stripe_cli:
    stripe listen --forward-to 0.0.0.0:8000/order/stripe/webhook/
