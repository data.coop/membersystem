# data.coop member system

## Development setup

There are two ways to set up the development environment.

- Using the Docker Compose setup provided in this repository.
- Using [uv](https://docs.astral.sh/uv/) in your host OS.

### Using Docker Compose

Working with the Docker Compose setup is made easy with the `Justfile` provided in the repository.

#### Requirements

- Docker
- docker compose plugin
- Just CLI (https://github.com/casey/just?tab=readme-ov-file#packages)

#### Setup

1. Setup .env file

    An example .env file is provided in the repository. You can copy it to .env file using the following command:

    ```bash
    cp .env.example .env
    ```

    The default values in the .env file are suitable for the docker-compose setup.

2. Migrate

    ```bash
    just manage migrate
    ```

3. Run the development server

    ```bash
    just run
    ```

#### Building and running other things

```bash
# Build the containers
just build

# Create a superuser
just manage createsuperuser

# Create Django migrations (after this, maybe you need to change file permissions in volume)
just manage makemigrations
```

### Using uv

#### Requirements

- Python 3.12 or higher
- [uv](https://docs.astral.sh/uv/)
- A running PostgreSQL server

#### Setup

1. Setup .env file

    An example .env file is provided in the repository. You can copy it to .env file using the following command:

    ```bash
    cp .env.example .env
    ```

    Edit the .env file and set the values for the environment variables, especially the database variables.

2. Run migrate

    ```bash
    uv run src/manage.py migrate
    ```

3. Run the development server

   ```bash
    uv run src/manage.py runserver
   ```

### Updating requirements

We use uv. That means we have a set of loosely defined `dependencies` in `pyproject.toml` and lock dependencies in `uv.lock`.

To generate `uv.lock` run:

```bash
# Build requirements.txt etc
uv lock

# Build Docker image with new Python requirements
just build
```

## Important notes

* This project uses [django-zen-queries](https://github.com/dabapps/django-zen-queries), which will sometimes raise a `QueriesDisabledError` in your templates. You can find a difference of opinion about that, but you can find a difference of opinion about many things, right?
* If a linting error annoys you, please feel free to strike back by adding a `noqa` to the line that has displeased the linter and move on with life.
