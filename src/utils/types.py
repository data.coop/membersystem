"""Collection of types for the project."""

from django.contrib.auth.models import User
from django.http import HttpRequest


class AuthenticatedHttpRequest(HttpRequest):
    """HttpRequest with an authenticated user."""

    user: User
