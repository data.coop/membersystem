"""Command to bootstrap development data."""

from datetime import timedelta

from accounting.models import Product
from django.contrib.auth.models import User
from django.core.management import BaseCommand
from django.db.backends.postgresql.psycopg_any import DateRange
from django.utils import timezone
from djmoney.money import Money
from membership.models import MembershipType
from membership.models import SubscriptionPeriod


class Command(BaseCommand):
    """Command to bootstrap development data."""

    help = "Bootstrap dev data"

    superuser: User
    normal_users: list[User]

    products: dict[str, Product]

    def handle(self, *args: str, **options: str) -> None:
        """Handle the command."""
        self.create_superuser()
        self.create_normal_users()
        self.create_subscription_periods()
        self.create_products()
        self.create_membership_types()

    def create_superuser(self) -> None:
        """Create superuser."""
        self.stdout.write("Creating superuser")
        self.superuser = User.objects.create_superuser("admin", "admin@example.com")
        self.superuser.set_password("admin")
        self.superuser.save()

    def create_normal_users(self) -> None:
        """Create normal users."""
        self.stdout.write("Creating normal users")
        self.normal_users = [User.objects.create_user(f"user{i}", email=f"user{i}@example.com") for i in range(1, 4)]
        for user in self.normal_users:
            user.set_password(user.username)
            user.save()

    def create_subscription_periods(self) -> None:
        """Create subscription periods."""
        self.stdout.write("Creating subscription periods")
        SubscriptionPeriod.objects.create(
            period=DateRange(timezone.now().date() - timedelta(days=182), timezone.now().date() + timedelta(days=183))
        )
        SubscriptionPeriod.objects.create(
            period=DateRange(timezone.now().date() + timedelta(days=183), timezone.now().date() + timedelta(days=365))
        )

    def create_products(self) -> None:
        """Create products."""
        self.stdout.write("Creating products")
        products_data = {
            "Medlemsydelse": (360, 90),
            "Medlemskontingent": (150, 0),
            "Nedsat medlemsydelse": (40, 10),
            "Nedsat medlemskontingent": (50, 0),
        }
        self.products = {}
        for name, (price, vat) in products_data.items():
            self.products[name] = Product.objects.create(
                name=name,
                price=Money(price, "DKK"),
                vat=Money(vat, "DKK"),
            )

    def create_membership_types(self) -> None:
        """Create membership types."""
        self.stdout.write("Creating membership types")
        ydelse = self.products["Medlemsydelse"]
        kontingent = self.products["Medlemskontingent"]
        nedsat_ydelse = self.products["Nedsat medlemsydelse"]
        nedsat_kontingent = self.products["Nedsat medlemskontingent"]
        MembershipType.objects.create(name="Normalt medlemskab").products.add(ydelse, kontingent)
        MembershipType.objects.create(name="Nedsat medlemskab").products.add(nedsat_ydelse, nedsat_kontingent)
