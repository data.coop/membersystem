"""Accounting app configuration."""

from django.apps import AppConfig


class AccountingConfig(AppConfig):
    """Accounting app config."""

    name = "accounting"

    def ready(self) -> None:
        """Implicitly connect a signal handlers decorated with @receiver."""
        from . import signals  # noqa: F401
