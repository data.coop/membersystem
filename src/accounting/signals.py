"""Loaded with the AppConfig.ready() method."""

from django.core.mail import mail_admins
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from membership.models import Membership

from . import models


# method for updating
@receiver(post_save, sender=models.Payment)
def check_total_amount(sender: models.Payment, instance: models.Payment, **kwargs: dict) -> None:
    """Check that we receive Payments with the correct amount."""
    if instance.amount != instance.order.total_with_vat:
        mail_admins(
            "Payment received: wrong amount",
            f"Please check payment ID {instance.pk}",
        )


@receiver(post_save, sender=models.Payment)
def mark_order_paid(sender: models.Payment, instance: models.Payment, **kwargs: dict) -> None:
    """Mark an order as paid when payment is received."""
    instance.order.is_paid = True
    instance.order.save()


@receiver(post_save, sender=models.Order)
def activate_membership(sender: models.Order, instance: models.Order, **kwargs: dict) -> None:
    """Mark a membership as activated when its order is marked as paid."""
    if instance.is_paid:
        Membership.objects.filter(order=instance, activated=False, activated_on=None).update(
            activated=True, activated_on=timezone.now()
        )
