# Generated by Django 5.0.7 on 2024-07-21 14:12

import django.db.models.deletion
import djmoney.models.fields
from django.db import migrations
from django.db import models


class Migration(migrations.Migration):
    dependencies = [
        ("accounting", "0003_alter_payment_stripe_charge_id"),
    ]

    operations = [
        migrations.CreateModel(
            name="PaymentType",
            fields=[
                ("id", models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("modified", models.DateTimeField(auto_now=True, verbose_name="modified")),
                ("created", models.DateTimeField(auto_now_add=True, verbose_name="created")),
                ("name", models.CharField(max_length=1024, verbose_name="description")),
                ("description", models.TextField(blank=True, max_length=2048)),
                ("enabled", models.BooleanField(default=True)),
            ],
            options={
                "abstract": False,
            },
        ),
        migrations.CreateModel(
            name="Product",
            fields=[
                ("id", models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("modified", models.DateTimeField(auto_now=True, verbose_name="modified")),
                ("created", models.DateTimeField(auto_now_add=True, verbose_name="created")),
                ("name", models.CharField(max_length=512)),
                (
                    "price_currency",
                    djmoney.models.fields.CurrencyField(
                        choices=[("DKK", "DKK")], default=None, editable=False, max_length=3
                    ),
                ),
                ("price", djmoney.models.fields.MoneyField(decimal_places=2, max_digits=16)),
                (
                    "vat_currency",
                    djmoney.models.fields.CurrencyField(
                        choices=[("DKK", "DKK")], default=None, editable=False, max_length=3
                    ),
                ),
                ("vat", djmoney.models.fields.MoneyField(decimal_places=2, max_digits=16)),
            ],
            options={
                "abstract": False,
            },
        ),
        migrations.AddField(
            model_name="payment",
            name="external_transaction_id",
            field=models.CharField(blank=True, default="", max_length=255),
        ),
        migrations.AlterField(
            model_name="payment",
            name="stripe_charge_id",
            field=models.CharField(blank=True, default="", max_length=255),
        ),
        migrations.AddField(
            model_name="payment",
            name="payment_type",
            field=models.ForeignKey(
                default=1, on_delete=django.db.models.deletion.PROTECT, to="accounting.paymenttype"
            ),
            preserve_default=False,
        ),
        migrations.CreateModel(
            name="OrderProduct",
            fields=[
                ("id", models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("modified", models.DateTimeField(auto_now=True, verbose_name="modified")),
                ("created", models.DateTimeField(auto_now_add=True, verbose_name="created")),
                (
                    "price_currency",
                    djmoney.models.fields.CurrencyField(
                        choices=[("DKK", "DKK")], default=None, editable=False, max_length=3
                    ),
                ),
                ("price", djmoney.models.fields.MoneyField(decimal_places=2, max_digits=16)),
                (
                    "vat_currency",
                    djmoney.models.fields.CurrencyField(
                        choices=[("DKK", "DKK")], default=None, editable=False, max_length=3
                    ),
                ),
                ("vat", djmoney.models.fields.MoneyField(decimal_places=2, max_digits=16)),
                (
                    "order",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="ordered_products",
                        to="accounting.order",
                    ),
                ),
                (
                    "product",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        related_name="ordered_products",
                        to="accounting.product",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
    ]
