"""Config for the services app."""

from typing import Any

from django.apps import AppConfig
from django.apps import apps
from django.db.models.signals import post_migrate


def ensure_administrator_group(sender: Any, **kwargs: Any) -> None:  # noqa: ANN401
    """Ensure that a group called "administrators" exists."""
    group_model = apps.get_model("auth.Group")
    group_model.objects.get_or_create(name="administrators")


class ServicesConfig(AppConfig):
    """Config for the services app."""

    default_auto_field = "django.db.models.BigAutoField"
    name = "services"

    def ready(self) -> None:
        """Connect signals when apps are ready."""
        post_migrate.connect(ensure_administrator_group, sender=self)
