"""Admin for the services app."""

from django.contrib import admin

from services.models import ServiceAccess
from services.models import ServiceRequest


@admin.register(ServiceRequest)
class ServiceRequestAdmin(admin.ModelAdmin):
    """Admin for the ServiceRequest model."""

    list_display = ("created", "member", "service", "request", "status", "assigned_to")
    list_filter = ("request", "status", "assigned_to")
    sortable_by = ("created",)


@admin.register(ServiceAccess)
class ServiceAccessAdmin(admin.ModelAdmin):
    """Admin for ServiceAccess model."""
