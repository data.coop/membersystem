"""Registry for services."""

from django import forms
from django.db.models import TextChoices
from django.utils.translation import gettext_lazy as _
from django_registries.registry import Interface
from django_registries.registry import Registry


class ServiceRegistry(Registry["ServiceInterface"]):
    """Registry for services."""

    implementations_module = "services"


class ServiceRequests(TextChoices):
    """Service request choices."""

    CREATION = "CREATION", _("Creation")
    PASSWORD_RESET = "PASSWORD_RESET", _("Password reset")
    DELETION = "DELETION", _("Deletion")


DEFAULT_SERVICE_REQUEST_TYPES = [ServiceRequests.CREATION, ServiceRequests.PASSWORD_RESET, ServiceRequests.DELETION]


class ServiceInterface(Interface):
    """Interface for services."""

    registry = ServiceRegistry

    name: str
    description: str
    url: str

    public: bool = False

    # An auto-created service is added for all new members.
    # This is a way of saying that the service is "mandatory" to have.
    auto_create: bool = False

    request_types: list[ServiceRequests] = DEFAULT_SERVICE_REQUEST_TYPES

    subscribe_fields: tuple[tuple[str, forms.Field], ...] = ()

    def get_form_class(self) -> type:
        """Get the form class for the service."""
        return type(
            "ServiceForm",
            (forms.Form,),
            dict(self.subscribe_fields),
        )

    def __str__(self) -> str:
        return self.name
