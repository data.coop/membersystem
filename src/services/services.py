"""Service classes for data.coop."""

from django import forms

from .registry import ServiceInterface


class MailService(ServiceInterface):
    """Mail service."""

    slug = "mail"
    name = "Mail"
    url = "https://mail.data.coop"
    description = "Mail service for data.coop"

    subscribe_fields = (("username", forms.CharField()),)


class MatrixService(ServiceInterface):
    """Matrix service."""

    slug = "matrix"
    name = "Matrix"
    url = "https://matrix.data.coop"
    description = "Matrix service for data.coop"

    subscribe_fields = (("username", forms.CharField()),)

    auto_create = True


class MastodonService(ServiceInterface):
    """Mastodon service."""

    slug = "mastodon"
    name = "Mastodon"
    url = "https://social.data.coop"
    description = "Mastodon service for data.coop"

    subscribe_fields = (("username", forms.CharField()),)


class NextcloudService(ServiceInterface):
    """Nextcloud service."""

    slug = "nextcloud"
    name = "Nextcloud"
    url = "https://cloud.data.coop"
    description = "Nextcloud service for data.coop"


class HedgeDocService(ServiceInterface):
    """HedgeDoc service."""

    slug = "hedgedoc"
    name = "HedgeDoc"
    url = "https://pad.data.coop"
    public = True
    description = "HedgeDoc service for data.coop"


class ForgejoService(ServiceInterface):
    """Forgejo service."""

    slug = "forgejo"
    name = "Forgejo"
    url = "https://git.data.coop"
    description = "Git service for data.coop"
    auto_create = True


class RalllyService(ServiceInterface):
    """Rallly service."""

    slug = "rallly"
    name = "Rallly"
    url = "https://when.data.coop"
    public = True
    description = "Rallly service for data.coop"
