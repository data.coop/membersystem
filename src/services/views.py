"""Views for the services app."""

from __future__ import annotations

from typing import TYPE_CHECKING

from django.shortcuts import redirect
from django_view_decorator import namespaced_decorator_factory
from utils.view_utils import render

from services.models import ServiceAccess
from services.registry import ServiceInterface
from services.registry import ServiceRegistry

if TYPE_CHECKING:
    from django.contrib.auth.models import User
    from django.http import HttpResponse
    from utils.types import AuthenticatedHttpRequest

services_view = namespaced_decorator_factory(
    namespace="services",
    base_path="services",
)


@services_view(
    paths="",
    name="list",
    login_required=True,
)
def services_overview(request: AuthenticatedHttpRequest) -> HttpResponse:
    """View all services."""
    active_services = get_services(user=request.user)

    active_service_classes = [service.__class__ for service in active_services]

    non_active_services = [
        service for _, service in ServiceRegistry.get_items() if service not in active_service_classes
    ]

    context = {
        "non_active_services": non_active_services,
        "active_services": active_services,
    }

    return render(
        request=request,
        template_name="services/services_overview.html",
        context=context,
    )


@services_view(
    paths="<str:service_slug>/",
    name="detail",
    login_required=True,
)
def service_detail(request: AuthenticatedHttpRequest, service_slug: str) -> HttpResponse:
    """View a service."""
    service = ServiceRegistry.get(slug=service_slug)

    context = {
        "service": service,
    }

    return render(
        request=request,
        template_name="services/service_detail.html",
        context=context,
    )


@services_view(
    paths="<str:service_slug>/subscribe/",
    name="subscribe",
    login_required=True,
)
def service_subscribe(request: AuthenticatedHttpRequest, service_slug: str) -> HttpResponse:
    """Subscribe to a service."""
    service = ServiceRegistry.get(slug=service_slug)

    form_class = service.get_form_class()

    if request.method == "POST":
        form = form_class(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            service_access = ServiceAccess(
                member=request.user,
                service=service.slug,
                subscription_data=data,
            )
            service_access.save()
            return redirect("services:list")

    context = {
        "service": service,
        "base_path": "services:list",
        "form": form_class(),
    }

    return render(
        request=request,
        template_name="services/service_subscribe.html",
        context=context,
    )


def get_services(*, user: User) -> list[ServiceInterface]:
    """Get services for the user."""
    return [
        access.service_implementation
        for access in ServiceAccess.objects.filter(
            member=user,
        )
    ]
