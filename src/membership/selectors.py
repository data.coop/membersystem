"""Selectors for the membership app."""

from __future__ import annotations

import contextlib
from typing import TYPE_CHECKING

from django.db.models import Exists
from django.db.models import OuterRef
from django.utils import timezone

from membership.models import Member
from membership.models import Membership
from membership.models import SubscriptionPeriod

if TYPE_CHECKING:
    from django.db.models.query import QuerySet


def get_subscription_periods(member: Member | None = None) -> list[SubscriptionPeriod]:
    """Get all subscription periods."""
    subscription_periods = SubscriptionPeriod.objects.prefetch_related(
        "membership_set",
        "membership_set__user",
    ).all()

    if member:
        subscription_periods = subscription_periods.annotate(
            membership_exists=Exists(
                Membership.objects.filter(
                    user=member,
                    period=OuterRef("pk"),
                ),
            ),
        ).filter(membership_exists=True)

    return list(subscription_periods)


def get_current_subscription_period() -> SubscriptionPeriod | None:
    """Get the current subscription period."""
    with contextlib.suppress(SubscriptionPeriod.DoesNotExist):
        return SubscriptionPeriod.objects.prefetch_related(
            "membership_set",
            "membership_set__user",
        ).get(period__contains=timezone.now())


def get_memberships(
    *,
    member: Member | None = None,
    period: SubscriptionPeriod | None = None,
) -> Membership.QuerySet:
    """Get memberships."""
    memberships = Membership.objects.select_related("membership_type").all()

    if member:
        memberships = memberships.for_member(member=member)

    if period:
        memberships = memberships.filter(period=period)

    return memberships


def get_members() -> QuerySet[Member]:
    """Get all members."""
    return Member.objects.all().annotate_membership().order_by("username")


def get_member(*, member_id: int) -> Member:
    """Get a member by id."""
    return get_members().get(id=member_id)
