"""Models for the membership app."""

import uuid
from typing import Self

from dirtyfields import DirtyFieldsMixin
from django.contrib.auth.models import User
from django.contrib.auth.models import UserManager
from django.contrib.postgres.constraints import ExclusionConstraint
from django.contrib.postgres.fields import DateRangeField
from django.contrib.postgres.fields import RangeOperators
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext as _
from djmoney.money import Money
from services.models import ServiceRequest
from utils.mixins import CreatedModifiedAbstract


class NoSubscriptionPeriodFoundError(Exception):
    """Raised when no subscription period is found."""


class Member(User):
    """Proxy model for the User model to add some convenience methods."""

    class QuerySet(models.QuerySet):
        """QuerySet for the Member model."""

        def annotate_membership(self) -> Self:
            """Annotate whether the user has an active membership."""
            from .selectors import get_current_subscription_period

            current_subscription_period = get_current_subscription_period()

            if not current_subscription_period:
                raise NoSubscriptionPeriodFoundError

            return self.annotate(
                active_membership=models.Exists(
                    Membership.objects.filter(
                        user=models.OuterRef("pk"),
                        period=current_subscription_period.id,
                    ),
                ),
            )

    objects = UserManager.from_queryset(QuerySet)()

    def get_display_name(self) -> str:
        """Choose how to display the user in emails and UI and ultimately to other users.

        It's crucial that we currently don't have a good solution for this.
        We should allow the user to define their own nick.
        """
        return self.username

    @property
    def language_code(self) -> str:
        """Returns the user's preferred language code.

        We don't have an actual setting for this... because this is a proxy table.
        """
        return "da-dk"

    class Meta:
        proxy = True


class SubscriptionPeriod(CreatedModifiedAbstract):
    """A subscription period.

    Denotes a period for which members should pay their membership fee for.
    """

    class QuerySet(models.QuerySet):
        """QuerySet for the Membership model."""

        def _current(self) -> Self:
            """Filter memberships for the current period."""
            return self.filter(period__contains=timezone.now())

        def current(self) -> "Membership | None":
            """Get the current membership."""
            try:
                return self._current().get()
            except self.model.DoesNotExist:
                return None

    objects = QuerySet.as_manager()

    period = DateRangeField(verbose_name=_("period"))

    class Meta:
        constraints: (
            ExclusionConstraint(
                name="exclude_overlapping_periods",
                expressions=[
                    ("period", RangeOperators.OVERLAPS),
                ],
            ),
        )

    def __str__(self) -> str:
        return f"{self.period.lower} - {self.period.upper or _('next general assembly')}"


class Membership(DirtyFieldsMixin, CreatedModifiedAbstract):
    """A membership.

    Tracks that a user has membership of a given type for a given period.
    """

    class QuerySet(models.QuerySet):
        """QuerySet for the Membership model."""

        def for_member(self, member: Member) -> Self:
            """Filter memberships for a given member."""
            return self.filter(user=member)

        def active(self) -> Self:
            """Get only activated, non-revoked memberships (may have expired so use also current())."""
            return self.filter(activated=True, revoked=False)

        def _current(self) -> Self:
            """Filter memberships for the current period."""
            return self.filter(period__period__contains=timezone.now())

        def current(self) -> "Membership | None":
            """Get the current membership."""
            return self._current().first()

        def previous(self) -> list["Membership"]:
            """Get previous memberships."""
            # A naïve way to get previous by just excluding the current. This
            # means that there must be some protection against "future"
            # memberships.
            return list(self.all().difference(self._current()))

    objects = QuerySet.as_manager()

    user = models.ForeignKey("auth.User", on_delete=models.PROTECT, related_name="memberships")

    # This code is used for inviting a user to create an account for this membership.
    referral_code = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)

    membership_type = models.ForeignKey(
        "membership.MembershipType",
        related_name="memberships",
        verbose_name=_("membership type"),
        on_delete=models.PROTECT,
    )

    period = models.ForeignKey(
        "membership.SubscriptionPeriod",
        on_delete=models.PROTECT,
    )

    order = models.ForeignKey(
        "accounting.Order",
        null=True,
        blank=True,
        verbose_name=_("order"),
        help_text=_("The order filled in for paying this membership."),
        on_delete=models.PROTECT,
    )

    activated = models.BooleanField(
        default=False, verbose_name=_("activated"), help_text=_("Membership was activated.")
    )
    activated_on = models.DateTimeField(null=True, blank=True)

    revoked = models.BooleanField(
        default=False,
        verbose_name=_("revoked"),
        help_text=_(
            "Membership has explicitly been revoked. Revoking a membership is not associated with regular expiration "
            "of the membership period."
        ),
    )
    revoked_reason = models.TextField(blank=True)
    revoked_on = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = _("membership")
        verbose_name_plural = _("memberships")

    def __str__(self) -> str:
        return f"{self.user} - {self.period}"

    def save(self, *args, **kwargs) -> None:  # noqa: ANN002, ANN003
        """Override the standard save method."""
        is_new = not self.pk
        # A Membership is considered recently activated when:
        # It was created w/ activated=True OR
        # It was changed from activated=False to activated=True
        # We use django-dirtyfields to detect changes to fields
        is_activated = self.activated and (is_new or not self.get_dirty_fields().get("activated", False))
        super().save(*args, **kwargs)
        # When a membership is activated, we should create service requests
        if is_activated:
            ServiceRequest.create_defaults(membership=self)


class MembershipType(CreatedModifiedAbstract):
    """A membership type.

    Models membership types. Currently only a name, but will in the future
    possibly contain more information like fees.
    """

    name = models.CharField(verbose_name=_("name"), max_length=64)

    products = models.ManyToManyField("accounting.Product")

    active = models.BooleanField(default=True)

    class Meta:
        verbose_name = _("membership type")
        verbose_name_plural = _("membership types")

    def __str__(self) -> str:
        return self.name

    def create_membership(self, user: User) -> Membership:
        """Create a current membership for this type."""
        from .selectors import get_current_subscription_period

        return Membership.objects.create(
            membership_type=self,
            user=user,
            period=get_current_subscription_period(),
        )

    @property
    def total_including_vat(self) -> Money:
        """Calculate the total price of this membership (including VAT)."""
        return sum(product.price + product.vat for product in self.products.all())


class WaitingListEntry(CreatedModifiedAbstract):
    """People who for some reason could want to be added to a waiting list and invited to join later."""

    name = models.CharField(verbose_name=_("name"), max_length=255)
    email = models.EmailField()
    username = models.CharField(verbose_name=_("username"), max_length=150, blank=True, default="")
    geography = models.CharField(verbose_name=_("geography"), blank=True, default="")
    comment = models.TextField(blank=True)
    wants_introduction = models.BooleanField(
        verbose_name=_("wants introduction"),
        default=False,
        help_text=_("Whether the person wants an introduction to the association."),
    )
    membership_type = models.ForeignKey(
        MembershipType,
        verbose_name=_("membership type"),
        help_text=_("The membership type the person wants to apply for."),
        on_delete=models.PROTECT,
        limit_choices_to=models.Q(active=True),
    )
    member = models.ForeignKey(
        Member,
        null=True,
        blank=True,
        verbose_name=_("has member"),
        help_text=_("Once a member account is generated (use the admin action), this field will be marked."),
        on_delete=models.CASCADE,
    )

    def __str__(self) -> str:
        return self.email

    @classmethod
    def get_applications_by_month(cls) -> dict[str, int]:
        """Return a dictionary with the number of applications by month."""
        from django.db.models import Count
        from django.db.models.functions import TruncMonth

        return dict(
            cls.objects.annotate(month=TruncMonth("created"))
            .values("month")
            .annotate(count=Count("id"))
            .order_by("-month")
            .values_list("month", "count")
        )

    class Meta:
        verbose_name = _("waiting list entry")
        verbose_name_plural = _("waiting list entries")
