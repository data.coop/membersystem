"""Permissions for the membership app."""

from __future__ import annotations

from dataclasses import dataclass
from typing import TYPE_CHECKING

from django.contrib.auth.models import Permission as DjangoPermission
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import gettext_lazy as _

if TYPE_CHECKING:
    from django_stubs_ext import StrOrPromise

PERMISSIONS: list[Permission] = []


def persist_permissions(*args, **kwargs) -> None:  # type: ignore[no-untyped-def] # noqa: ANN002, ANN003
    """Persist all permissions."""
    for permission in PERMISSIONS:
        permission.persist_permission()


@dataclass
class Permission:
    """Dataclass to define a permission."""

    name: StrOrPromise
    codename: str
    app_label: str
    model: str

    def __post_init__(self, *args, **kwargs) -> None:  # noqa: ANN002, ANN003
        """Post init method."""
        PERMISSIONS.append(self)

    @property
    def path(self) -> str:
        """Return the path of the permission."""
        return f"{self.app_label}.{self.codename}"

    def persist_permission(self) -> None:
        """Persist the permission."""
        content_type, _ = ContentType.objects.get_or_create(
            app_label=self.app_label,
            model=self.model,
        )
        DjangoPermission.objects.get_or_create(
            content_type=content_type,
            codename=self.codename,
            defaults={"name": self.name},
        )


ADMINISTRATE_MEMBERS = Permission(
    name=_("Can administrate members"),
    codename="administrate_members",
    app_label="membership",
    model="membership",
)
