"""Membership app configuration."""

from django.apps import AppConfig
from django.db.models.signals import post_migrate


class MembershipConfig(AppConfig):
    """Membership app config."""

    name = "membership"

    def ready(self) -> None:
        """Ready method."""
        from .permissions import persist_permissions

        post_migrate.connect(persist_permissions, sender=self)
