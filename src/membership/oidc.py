"""Code related to OIDC."""

from typing import Any

from oauth2_provider.oauth2_validators import OAuth2Validator
from utils.types import AuthenticatedHttpRequest


class CustomOAuth2Validator(OAuth2Validator):
    """A custom OAuth2 validator."""

    def get_additional_claims(self, request: AuthenticatedHttpRequest) -> dict[str, Any]:
        """Get additional claims."""
        return {
            "sub": request.user.email,
            "email": request.user.email,
            "first_name": request.user.first_name,
            "last_name": request.user.last_name,
        }
