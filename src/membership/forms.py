"""Form for the membership app."""

from __future__ import annotations

from typing import TYPE_CHECKING

from allauth.account.adapter import get_adapter as get_allauth_adapter
from allauth.account.forms import SetPasswordForm
from django import forms
from django.utils.translation import gettext_lazy as _

from .models import MembershipType
from .models import WaitingListEntry

if TYPE_CHECKING:
    from typing import Any
    from typing import ClassVar

    from django_stubs_ext import StrOrPromise


class InviteForm(SetPasswordForm):
    """Create a new password for a user account that is created through an invite."""

    username = forms.CharField(
        label=_("Username"),
        widget=forms.TextInput(attrs={"placeholder": _("Username"), "autocomplete": "username"}),
    )

    def __init__(self, *args, **kwargs) -> None:  # noqa: ANN002, ANN003
        self.membership = kwargs.pop("membership")
        kwargs["user"] = self.membership.user
        super().__init__(*args, **kwargs)

    def clean_username(self) -> str:
        """Clean the username value.

        Taken from the allauth Signup form - we should consider that data can be leaked here.
        """
        value = self.cleaned_data["username"]
        # The allauth adapter ensures the username is unique.
        return get_allauth_adapter().clean_username(value)

    def save(self) -> None:
        """Save instance to db.

        Note: You can hack a re-activation of a deactivated account
        by getting a valid token before deactivation (from the reset password form).
        We can block this by also setting Membership.revoked=False when deactivating someone's account.
        """
        self.user.username = self.cleaned_data["username"]
        self.user.is_active = True
        self.user.save()
        super().save()


class MemberApplicationForm(forms.ModelForm):
    """Form for applying for membership."""

    accepted_aup = forms.BooleanField(
        label=_("I have read and accepted the Acceptable Usage Policy."),
        required=False,
    )
    has_read_bylaws = forms.BooleanField(
        label=_("I have read the bylaws."),
        required=False,
    )

    def __init__(self, *args: Any, **kwargs: Any) -> None:  # noqa: ANN401
        super().__init__(*args, **kwargs)
        # To evaluate the queryset before the form is rendered, we need to set the choices here.
        # Otherwise, django-zen-queries will complain about queries in the template.
        self.fields["membership_type"].choices = MembershipType.objects.filter(active=True).values_list("id", "name")

    class Meta:
        model = WaitingListEntry
        fields: ClassVar[list[str]] = [
            "name",
            "email",
            "username",
            "geography",
            "membership_type",
            "comment",
            "wants_introduction",
            "accepted_aup",
            "has_read_bylaws",
        ]
        labels: ClassVar[dict[str, StrOrPromise]] = {
            "username": _("Your preferred username"),
            "comment": _("Tell us about yourself"),
            "geography": _("Location"),
            "membership_type": _("Membership Type"),
            "wants_introduction": _("I would like an introduction to the association."),
            "accepted_aup": _("I have read and accepted the Acceptable Usage Policy."),
            "has_read_bylaws": _("I have read the bylaws."),
        }
        help_texts: ClassVar[dict[str, StrOrPromise]] = {
            "username": _("The username you would like to use."),
            "membership_type": _("Please select the membership type you are applying for."),
            "comment": _("Please provide a brief description about yourself and why you want to join."),
            "geography": _("Where are you located? This helps us organize local events."),
        }
        widgets: ClassVar[dict[str, forms.Widget]] = {
            "comment": forms.Textarea(attrs={"rows": 4}),
        }

    def clean(self) -> dict[str, str]:
        """Check that the user has read the bylaws."""
        cleaned_data = super().clean()
        if not cleaned_data.get("has_read_bylaws"):
            self.add_error("has_read_bylaws", _("You must read the bylaws before applying."))
        if not cleaned_data.get("accepted_aup"):
            self.add_error("accepted_aup", _("You must accept the AUP before applying."))
        return cleaned_data
