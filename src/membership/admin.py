"""Admin configuration for membership app."""

from collections.abc import Callable

from accounting.models import Account
from accounting.models import Order
from accounting.models import OrderProduct
from django.contrib import admin
from django.contrib import messages
from django.contrib.admin import ModelAdmin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.db import transaction
from django.db.models import QuerySet
from django.http import HttpRequest
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import path
from django.urls.resolvers import URLPattern
from django.utils.text import slugify

from .emails import InviteEmail
from .models import Member
from .models import Membership
from .models import MembershipType
from .models import SubscriptionPeriod
from .models import WaitingListEntry

# Do not use existing user admin
admin.site.unregister(User)


@admin.register(Membership)
class MembershipAdmin(admin.ModelAdmin):
    """Admin for Membership model."""

    list_display = ("user", "period", "membership_type", "activated", "revoked")
    list_filter = ("period", "membership_type", "activated", "revoked")
    search_fields = ("membership_type__name", "user__email", "user__first_name", "user__last_name")


@admin.register(MembershipType)
class MembershipTypeAdmin(admin.ModelAdmin):
    """Admin for MembershipType model."""


@admin.register(SubscriptionPeriod)
class SubscriptionPeriodAdmin(admin.ModelAdmin):
    """Admin for SubscriptionPeriod model."""


class MembershipInlineAdmin(admin.TabularInline):
    """Inline admin."""

    model = Membership


def decorate_ensure_membership_type_exists(membership_type: MembershipType, label: str) -> Callable:
    """Generate an admin action for given membership type and label."""

    @admin.action(description=label)
    def admin_action(modeladmin: ModelAdmin, request: HttpRequest, queryset: QuerySet) -> HttpResponse:
        return ensure_membership_type_exists(request, queryset, membership_type)

    return admin_action


@transaction.atomic
def ensure_membership_type_exists(
    request: HttpRequest,
    queryset: QuerySet[Member],
    membership_type: MembershipType,
) -> HttpResponse:
    """Inner function that ensures that a membership exists for a given queryset of Member objects."""
    for member in queryset:
        if member.memberships.filter(membership_type=membership_type).current():
            messages.info(request, f"{member} already has a membership {membership_type}")
        else:
            # Get the default account of the member. We don't really know what to do if a person owns multiple accounts.
            account, __ = Account.objects.get_or_create(owner=member)
            # Create an Order for the products in the membership
            order = Order.objects.create(member=member, account=account, description=membership_type.name)
            # Add stuff to the order
            for product in membership_type.products.all():
                OrderProduct.objects.create(order=order, product=product, price=product.price, vat=product.vat)
            # Create the Membership
            Membership.objects.create(
                membership_type=membership_type,
                user=member,
                period=SubscriptionPeriod.objects.current(),
                order=order,
            )

            # Associate the order with that membership
            messages.success(request, f"{member} has ordered a '{membership_type}' (unpaid)")


@admin.register(Member)
class MemberAdmin(UserAdmin):
    """Member admin is actually an admin for User objects."""

    inlines = (MembershipInlineAdmin,)
    actions: list[str | Callable] = ["send_invite"]  # noqa: RUF012
    list_display = ("email", "current_membership", "username", "is_staff", "is_active", "date_joined")

    @admin.display(description="membership")
    def current_membership(self, instance: Member) -> Membership | None:
        """Get the current membership for the member."""
        return instance.memberships.current()

    def get_actions(self, request: HttpRequest) -> dict:
        """Populate actions with dynamic data (MembershipType)."""
        current_period = SubscriptionPeriod.objects.current()

        super_dict = super().get_actions(request)

        if current_period:
            for i, mtype in enumerate(MembershipType.objects.filter(active=True)):
                action_label = f"Ensure membership {mtype.name}, {current_period.period}, {mtype.total_including_vat}"
                action_func = decorate_ensure_membership_type_exists(mtype, action_label)
                # Django ModelAdmin uses the non-unique __name__ property, so we need to suffix it to make it unique
                action_func.__name__ += f"_{i}"
                self.actions.append(action_func)

        return super_dict

    @admin.action(description="Send invite email to selected inactive accounts")
    def send_invite(self, request: HttpRequest, queryset: QuerySet[Member]) -> None:
        """Send invite email to the selected inactive accounts."""
        for member in queryset:
            if member.is_active:
                messages.error(
                    request,
                    f"Computer says no! This member will not receive an invite because the account is marked "
                    f"as active: {member.email}. That means the member has probably created a password and a username "
                    f"already, please tell them to use the password reminder function.",
                )
                continue
            if not member.memberships.current():
                messages.error(
                    request,
                    f"Computer says no! This member will not receive an invite because it has no current "
                    f"membership: {member.email}. You need to create a current membership before sending the invite.",
                )
                continue
            membership = member.memberships.current()
            email = InviteEmail(membership, request)
            email.send()
            messages.success(request, f"Sent an invitation to: {member.email}")


@admin.register(WaitingListEntry)
class WaitingListEntryAdmin(admin.ModelAdmin):
    """Admin for WaitingList model."""

    list_display = ("email", "name", "geography", "created", "wants_introduction", "member")
    list_filter = ("wants_introduction", "created", "geography")
    search_fields = ("email", "name", "comment", "geography")
    readonly_fields = ("created", "modified")
    actions = ("create_member",)

    def get_urls(self) -> list[URLPattern]:
        """Add custom URLs to the admin."""
        urls = super().get_urls()
        custom_urls = [
            path(
                "statistics/",
                self.admin_site.admin_view(self.application_statistics_view),
                name="membership_waitinglistentry_statistics",
            ),
        ]
        return custom_urls + urls

    def application_statistics_view(self, request: HttpRequest) -> HttpResponse:
        """View to display application statistics."""
        applications_by_month = WaitingListEntry.get_applications_by_month()

        context = {
            **self.admin_site.each_context(request),
            "title": "Application Statistics",
            "applications_by_month": applications_by_month,
            "opts": self.model._meta,  # noqa: SLF001
        }

        return render(request, "admin/membership/waitinglistentry/statistics.html", context)

    def changelist_view(self, request: HttpRequest, extra_context: dict | None = None) -> HttpResponse:
        """Add a link to the statistics view."""
        extra_context = extra_context or {}
        extra_context["show_statistics_link"] = True
        return super().changelist_view(request, extra_context)

    @admin.action(description="Create member account for entries")
    def create_member(self, request: HttpRequest, queryset: QuerySet[WaitingListEntry]) -> None:
        """Create a user account for this entry.

        Note that actions can soon be made available from the edit page, too:
        https://github.com/django/django/pull/16012
        """
        for entry in queryset:
            member = Member.objects.create_user(email=entry.email, username=slugify(entry.email), is_active=False)
            entry.member = member
            entry.save()
            messages.info(
                request,
                f"Added user for {entry.email} - ensure they have a membership and send an invite email.",
            )
