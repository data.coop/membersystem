"""Project views."""

from __future__ import annotations

from typing import TYPE_CHECKING

from accounting.models import Order
from django_view_decorator import view
from utils.view_utils import render

if TYPE_CHECKING:
    from django.http import HttpRequest
    from django.http import HttpResponse


@view(
    paths="",
    name="index",
    login_required=True,
)
def index(request: HttpRequest) -> HttpResponse:
    """View to show the index page."""
    unpaid_orders = Order.objects.filter(member=request.user, is_paid=False)

    context = {"unpaid_orders": list(unpaid_orders)}

    return render(request, "index.html", context=context)
