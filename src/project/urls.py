"""URLs for the membersystem."""

from django.conf import settings
from django.contrib import admin
from django.urls import include
from django.urls import path
from django_view_decorator import include_view_urls

urlpatterns = [
    path("", include_view_urls(extra_modules=["project.views"])),
    path("o/", include("oauth2_provider.urls", namespace="oauth2_provider")),
    path("accounts/", include("allauth.urls")),
    path("_admin/", admin.site.urls),
]

if settings.DEBUG:
    urlpatterns = [
        path("__debug__/", include("debug_toolbar.urls")),
        path("__reload__/", include("django_browser_reload.urls")),
        *urlpatterns,
    ]
