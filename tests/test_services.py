"""Tests for services."""

import pytest
from membership.models import Membership
from services.models import ServiceRequest
from services.models import ServiceRequestStatus


@pytest.mark.django_db()
def test_membership_activation(active_membership: Membership):
    """Test membership activation."""
    assert ServiceRequest.objects.filter(
        member=active_membership.user,
        status=ServiceRequestStatus.NEW,
    ).exists()
