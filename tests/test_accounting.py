"""Tests for accounting."""

import pytest
from accounting import models
from django.contrib.auth.models import User


@pytest.mark.django_db()
def test_balance() -> None:
    """Test balance."""
    user = User.objects.create_user("test", "lala@adas.com", "1234")
    account = models.Account.objects.create(owner=user)
    assert account.balance == 0
