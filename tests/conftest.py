"""Pytest configuration."""

from datetime import timedelta
from unittest import mock

import pytest
from django.db.backends.postgresql.psycopg_any import DateRange
from django.utils import timezone
from membership.models import Member
from membership.models import Membership
from membership.models import MembershipType
from membership.models import SubscriptionPeriod


@pytest.fixture()
def membership_type():
    """Provide a membership type."""
    return MembershipType.objects.create(name="Test Membership Type")


@pytest.fixture()
def current_period():
    """Provide a current subscription period."""
    SubscriptionPeriod.objects.create(
        period=DateRange(timezone.now().date() - timedelta(days=182), timezone.now().date() + timedelta(days=183))
    )
    return SubscriptionPeriod.objects.current()


@pytest.fixture()
def active_membership(membership_type, current_period):
    """Provide an active membership."""
    member = Member.objects.create_user("test", "lala@adas.com", "1234")
    return Membership.objects.create(
        user=member,
        membership_type=membership_type,
        period=current_period,
        activated=True,
    )


@pytest.fixture(autouse=True)
def _mock_matrix_notify() -> None:
    """Mock the matrix notify post."""
    with mock.patch("utils.matrix.httpx.post"):
        yield
