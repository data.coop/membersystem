FROM ghcr.io/astral-sh/uv:python3.12-alpine

# - Silence uv complaining about not being able to use hard links,
# - tell uv to byte-compile packages for faster application startups,
# - prevent uv from accidentally downloading isolated Python builds,
# - pick a Python,
# - and finally declare `/app` as the target for `uv sync`.
ENV UV_LINK_MODE=copy \
    UV_COMPILE_BYTECODE=1 \
    UV_PYTHON_DOWNLOADS=never \
    UV_PYTHON=python3.12 \
    UV_PROJECT_ENVIRONMENT=/venv

ARG BUILD
ENV BUILD=${BUILD}
ARG DJANGO_ENV=production

WORKDIR /app

RUN --mount=type=cache,target=/root/.cache/uv \
    --mount=type=bind,source=uv.lock,target=uv.lock \
    --mount=type=bind,source=pyproject.toml,target=pyproject.toml <<EOF
mkdir -p /app/src/staticfiles
apk update
apk add --no-cache \
    binutils \
    libpq-dev \
    gettext \
    netcat-openbsd \
    postgresql-client
# run uv sync --no-dev if $DJANGO_ENV is production, otherwise run uv sync
if [ "$DJANGO_ENV" = "production" ]; then uv sync --frozen --no-install-project --no-dev; else uv sync --frozen --no-install-project; fi
EOF

COPY . .
ENV PATH="/venv/bin:$PATH"
RUN <<EOF
./src/manage.py compilemessages
./src/manage.py collectstatic --noinput
EOF

ENTRYPOINT ["/app/entrypoint.sh"]

EXPOSE 8000

CMD ["uvicorn", "project.asgi:application", "--host", "0.0.0.0", "--port", "8000", "--workers", "3", "--lifespan", "off", "--app-dir", "/app/src"]
